use std::collections::HashMap;

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
struct InnerStructure {}

#[derive(Debug, Clone, Deserialize)]
struct DataStructure {
    pub items: HashMap<String, InnerStructure>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("serde-yaml#232 example project");

    let filename: String = std::env::args()
        .nth(1)
        .or(Some("samples/sample-one.yaml".to_string()))
        .unwrap();

    println!("Loading file {file}", file = filename);

    let f = std::fs::File::open(filename)?;
    let d: DataStructure = serde_yaml::from_reader(f)?;

    println!("{:#?}", d);

    Ok(())
}
